
---2018/06/26 01:37:48---
Huronjilo has received 1 Rune Kiteshield from Lucifer
Huronjilo has received 1 Rune Platebody from Lucifer
Huronjilo has received 1 Rune Platelegs from Lucifer
Huronjilo has received 1519 Blood Money from Lucifer
Huronjilo has received 1 Rune Full Helm from Lucifer

---2018/06/26 01:38:44---
Huronjilo has received 927 Blood Money from Huronjil0

---2018/06/26 01:40:48---
Huronjilo has given 1 Rune Kiteshield to Huronjil0
Huronjilo has given 1 Rune Platebody to Huronjil0
Huronjilo has given 1 Rune Platelegs to Huronjil0
Huronjilo has given 1 Rune Full Helm to Huronjil0

---2018/06/26 10:39:06---
Huronjilo has given 250000 Coins to Huronjil0

---2018/06/26 14:36:00---
Huronjilo has given 1 Rapier to Huronjil0

---2018/06/26 17:12:13---
Huronjilo has given 1 Ring Of Wealth to Huronjil0

---2018/06/27 19:28:49---
Huronjilo has given 1675000 Coins to Huronjil0

---2018/06/27 19:28:49---
Huronjilo has received 1 Tuna Potato from Huronjil0

---2018/06/28 23:31:25---
Huronjilo has given 393969 Blood Money to Huronjil0

---2018/06/29 18:48:31---
Huronjilo has received 1 Bronze Santa Hat from Huronjil0

---2018/06/29 18:48:51---
Huronjilo has received 1 Casket (Medium) from Huronjil0

---2018/07/04 18:20:01---
Huronjilo has given 306454 Blood Money to Huronjil0

---2018/07/05 14:36:01---
Huronjilo has given 8500 Dark Crab to Huronjil0

---2018/07/05 15:01:39---
Huronjilo has given 162979 Blood Money to Huronjil0

---2018/07/06 12:29:25---
Huronjilo has received 8 Wyvern Bones from Huronjil0
Huronjilo has received 274 Dragon Bones from Huronjil0

---2018/07/07 15:19:19---
Huronjilo has received 400 Magic Logs from Fat Ghost

---2018/07/07 15:19:19---
Huronjilo has given 1200000 Coins to Fat Ghost

---2018/07/07 17:23:33---
Huronjilo has given 1 Dragon Boots to Huronjil0

---2018/07/07 17:26:27---
Huronjilo has given 172254 Blood Money to Huronjil0

---2018/07/08 16:49:24---
Huronjilo has received 1 Gilded Kiteshield from Huronjil0

---2018/07/08 18:09:38---
Huronjilo has given 1 Yew Shortbow (U) to Burgz
Huronjilo has given 1 Maple Longbow (U) to Burgz

---2018/07/08 18:09:38---
Huronjilo has received 100 Magic Logs from Burgz

---2018/07/08 21:07:05---
Huronjilo has given 1 Bandos D'Hide to Huronjil0
Huronjilo has given 1 Bandos Chaps to Huronjil0

---2018/07/09 09:35:34---
Huronjilo has received 67 Grimy Ranarr Weed from Huronjil0
Huronjilo has received 56 Grimy Irit Leaf from Huronjil0
Huronjilo has received 137 Grimy Kwuarm from Huronjil0
Huronjilo has received 45 Grimy Cadantine from Huronjil0
Huronjilo has received 77 Grimy Toadflax from Huronjil0
Huronjilo has received 64 Grimy Dwarf Weed from Huronjil0
Huronjilo has received 65 Grimy Harralander from Huronjil0

---2018/07/09 12:39:24---
Huronjilo has received 100 Lava Dragon Bones from Huronjil0
Huronjilo has received 130 Dragon Bones from Huronjil0

---2018/07/09 18:10:07---
Huronjilo has given 1 Armadyl Bracers to Huronjil0

---2018/07/10 15:35:06---
Huronjilo has received 133 Mithril Ore from Huronjil0
Huronjilo has received 69 Adamantite Ore from Huronjil0
Huronjilo has received 187 Runite Ore from Huronjil0
Huronjilo has received 1442 Coal from Huronjil0
Huronjilo has received 90 Iron Ore from Huronjil0
Huronjilo has received 700 Silver Ore from Huronjil0

---2018/07/10 17:50:06---
Huronjilo has received 1 Challenge Scroll (Medium) from Huronjil0

---2018/07/10 23:10:08---
Huronjilo has received 17 Uncut Dragonstone from Huronjil0

---2018/07/10 23:10:33---
Huronjilo has received 1 Uncut Onyx from Huronjil0

---2018/07/11 01:04:29---
Huronjilo has given 25 Morrigans Throwing Axe to Huronjil0

---2018/07/11 03:02:55---
Huronjilo has received 28 Runite Ore from Huronjil0
Huronjilo has received 40 Coal from Huronjil0
Huronjilo has received 296 Dragon Bones from Huronjil0
Huronjilo has received 128 Adamantite Bar from Huronjil0
Huronjilo has received 36 Runite Bar from Huronjil0

---2018/07/11 03:02:55---
Huronjilo has given 16 Clue Box to Huronjil0

---2018/07/11 09:17:28---
Huronjilo has received 71 Runite Ore from Huronjil0
Huronjilo has received 23 Adamantite Bar from Huronjil0
Huronjilo has received 11 Runite Bar from Huronjil0

---2018/07/11 09:19:24---
Huronjilo has received 467179 Blood Money from Huronjil0

---2018/07/12 23:31:58---
Huronjilo has received 450 Big Bones from Diablo

---2018/07/12 23:32:40---
Huronjilo has received 63 Babydragon Bones from Ranger

---2018/07/13 16:21:32---
Huronjilo has given 5000000 Coins to Hugepen15

---2018/07/13 23:53:07---
Huronjilo has received 30 Crystal Key from Huronjil0

---2018/07/15 21:22:56---
Huronjilo has received 26 Crystal Key from Huronjil0

---2018/07/16 11:52:59---
Huronjilo has received 80 Lava Dragon Bones from Huronjil0
Huronjilo has received 172 Dragon Bones from Huronjil0

---2018/07/16 13:50:37---
Huronjilo has given 490958 Blood Money to Huronjil0

---2018/07/16 17:49:13---
Huronjilo has received 600 Dragon Bones from Huronjil0

---2018/07/16 20:39:34---
Huronjilo has received 75 Dragon Bones from Huronjil0

---2018/07/17 11:12:01---
Huronjilo has received 3861 Pure Essence from Huronjil0

---2018/07/18 17:02:06---
Huronjilo has received 133791 Blood Money from Huronjil0

---2018/07/18 18:45:57---
Huronjilo has received 253 Grimy Dwarf Weed from Huronjil0

---2018/07/19 12:42:19---
Huronjilo has received 73 Castle Wars Ticket from Huronjil0

---2018/07/19 12:50:36---
Huronjilo has given 2164 Nature Rune to Huronjil0
Huronjilo has given 1864 Bolt Rack to Huronjil0

---2018/07/21 21:30:03---
Huronjilo has received 856 Coal from Fat Ghost

---2018/07/21 21:30:03---
Huronjilo has given 401000 Coins to Fat Ghost

---2018/07/21 21:31:56---
Huronjilo has received 292 Adamantite Ore from Huronjil0
Huronjilo has received 75 Runite Ore from Huronjil0
Huronjilo has received 1825 Coal from Huronjil0
Huronjilo has received 55 Adamantite Bar from Huronjil0
Huronjilo has received 26 Runite Bar from Huronjil0

---2018/07/22 11:07:59---
Huronjilo has received 4047 Coal from Huronjil0

---2018/07/22 11:09:06---
Huronjilo has given 12 Clue Box to Huronjil0

---2018/07/24 17:08:00---
Huronjilo has given 1 Casket (Medium) to Huronjil0

---2018/07/24 17:08:20---
Huronjilo has given 1 Challenge Scroll (Medium) to Huronjil0
Huronjilo has given 1 Casket (Medium) to Huronjil0

---2018/07/24 21:08:49---
Huronjilo has given 1 Monkey Nuts to Fat Ghost

---2018/07/24 21:13:22---
Huronjilo has given 28 Dragon Impling Jar to Fat Ghost

---2018/07/24 21:13:22---
Huronjilo has received 1 Monkey Nuts from Fat Ghost

---2018/07/26 10:09:32---
Huronjilo has received 50000 Blood Money from Bilbo

---2018/07/26 10:09:32---
Huronjilo has given 500 Adamantite Bar to Bilbo

---2018/07/26 16:29:38---
Huronjilo has given 21 Clue Box to Huronjil0

---2018/07/27 22:01:25---
Huronjilo has received 11 Castle Wars Ticket from Huronjil0

---2018/07/28 17:35:24---
Huronjilo has given 200 Dragon Impling Jar to Fat Ghost
Huronjilo has given 160 Dragon Arrow to Fat Ghost

---2018/07/28 17:41:26---
Huronjilo has received 69 Coins from Fat Ghost
Huronjilo has received 69 Blood Money from Fat Ghost
Huronjilo has received 55 Dragon Dart from Fat Ghost

---2018/07/29 17:39:11---
Huronjilo has received 343 Super Attack(4) from Huronjil0
Huronjilo has received 294 Super Strength(4) from Huronjil0
Huronjilo has received 588 Super Defence(4) from Huronjil0
Huronjilo has received 49 Grimy Torstol from Huronjil0

---2018/07/29 18:48:01---
Huronjilo has received 10 Crystal Key from Huronjil0

---2018/07/31 19:43:32---
Huronjilo has received 27 Pure Essence from Banks

---2018/07/31 19:44:34---
Huronjilo has received 2500 Pure Essence from Banks

---2018/08/01 15:19:15---
Huronjilo has received 18 Castle Wars Ticket from Patrity

---2018/08/01 15:19:15---
Huronjilo has given 180000 Blood Money to Patrity

---2018/08/02 12:43:28---
Huronjilo has received 22 Castle Wars Ticket from Reaxys

---2018/08/02 12:43:28---
Huronjilo has given 140000 Blood Money to Reaxys

---2018/08/02 17:37:47---
Huronjilo has given 80000 Blood Money to Burgz

---2018/08/02 17:37:47---
Huronjilo has received 9 Castle Wars Ticket from Burgz

---2018/08/04 18:41:27---
Huronjilo has received 1 Torva Full Helm from Huronjil0

---2018/08/04 18:44:09---
Huronjilo has received 1 Armadyl Godsword from Slayer

---2018/08/04 18:45:27---
Huronjilo has given 1 Torva Full Helm to Huronjil0
Huronjilo has given 1 Armadyl Godsword to Huronjil0

---2018/08/05 15:05:55---
Huronjilo has given 3457 Shark to Huronjil0
Huronjilo has given 365 Manta Ray to Huronjil0

---2018/08/05 15:20:37---
Huronjilo has given 500 Dragon Bones to Halee

---2018/08/05 21:24:31---
Huronjilo has received 2691779 Blood Money from Huronjil0

---2018/08/05 21:29:22---
Huronjilo has received 29 Crystal Key from Huronjil0

---2018/08/05 21:35:43---
Huronjilo has received 1 Ancestral Robe Bottom from Huronjil0
Huronjilo has received 2 Dark Bow from Huronjil0
Huronjilo has received 1 Rapier from Huronjil0
Huronjilo has received 1 Abyssal Tentacle from Huronjil0
Huronjilo has received 1 Vesta'S Longsword from Huronjil0
Huronjilo has received 1 Vesta'S Spear from Huronjil0
Huronjilo has received 1 Morrigans Top from Huronjil0
Huronjilo has received 1 Statius Platebody from Huronjil0
Huronjilo has received 2 Statius Platelegs from Huronjil0
Huronjilo has received 5 Statius Fullhelm from Huronjil0
Huronjilo has received 1 Dragon Hunter Crossbow from Huronjil0
Huronjilo has received 1 Torva Full Helm from Huronjil0
Huronjilo has received 1 Primordial Boots from Huronjil0
Huronjilo has received 1 Staff Of Light from Huronjil0
Huronjilo has received 1 Armadyl Godsword from Huronjil0
Huronjilo has received 1 Necklace Of Anguish from Huronjil0
Huronjilo has received 1 Bandos Boots from Huronjil0
Huronjilo has received 1 Ancestral Robe Top from Huronjil0

---2018/08/05 21:37:17---
Huronjilo has received 1 Saradomin'S Tear from Huronjil0
Huronjilo has received 1 Casket (Medium) from Huronjil0
Huronjilo has received 1 Gilded Scimitar from Huronjil0
Huronjilo has received 1 Zuriels Robetop from Huronjil0
Huronjilo has received 1 Samurai Shirt from Huronjil0
Huronjilo has received 1 Gilded Boots from Huronjil0
Huronjilo has received 1 Zuriels Robeskirt from Huronjil0
Huronjilo has received 1 Vesta'S Chainskirt from Huronjil0
Huronjilo has received 1 Samurai Greaves from Huronjil0
Huronjilo has received 1 Samurai Boots from Huronjil0
Huronjilo has received 1 Ranger Boots from Huronjil0
Huronjilo has received 1 Draconic Visage from Huronjil0
Huronjilo has received 1 Gilded Platebody from Huronjil0
Huronjilo has received 1 Gilded Platelegs from Huronjil0
Huronjilo has received 2 Challenge Scroll (Medium) from Huronjil0
Huronjilo has received 1 Gilded Full Helm from Huronjil0
Huronjilo has received 1 Casket (Medium) from Huronjil0

---2018/08/05 22:19:31---
Huronjilo has received 1 Ring Of Charos(A) from Huronjil0

---2018/08/06 13:52:50---
Huronjilo has given 5458 Death Rune to Huronjil0
Huronjilo has given 1 Ancestral Robe Bottom to Huronjil0
Huronjilo has given 20805 Blood Rune to Huronjil0
Huronjilo has given 1 Zuriels Robetop to Huronjil0
Huronjilo has given 1 Zuriels Robeskirt to Huronjil0
Huronjilo has given 1 Ancestral Robe Top to Huronjil0

---2018/08/06 14:20:17---
Huronjilo has given 1 Ring Of Charos(A) to Huronjil0

---2018/08/06 19:59:11---
Huronjilo has given 15000000 Coins to Slayer
Huronjilo has given 1 Anti-Dragon Shield to Slayer
Huronjilo has given 1 Draconic Visage to Slayer

---2018/08/06 20:00:01---
Huronjilo has received 1 Dragonfire Shield from Slayer

---2018/08/06 22:52:24---
Huronjilo has given 185 Dragon Dart to Huronjil0

---2018/08/07 14:27:48---
Huronjilo has received 1 Ourg Bones from Huronjil0
Huronjilo has received 1 Lava Dragon Bones from Huronjil0

---2018/08/07 14:28:41---
Huronjilo has received 1 Ourg Bones from Huronjil0

---2018/08/08 12:20:14---
Huronjilo has given 1 Dragon Hunter Crossbow to Burgz

---2018/08/08 12:20:14---
Huronjilo has received 1 Tuna Potato from Burgz

---2018/08/08 16:29:11---
Huronjilo has received 1 Amulet Of Fury from Huronjil0
Huronjilo has received 1 Fury Ornament Kit from Huronjil0

---2018/08/08 17:37:57---
Huronjilo has received 1 Karil'S Leatherskirt from Huronjil0
Huronjilo has received 1 Torva Platebody from Huronjil0
Huronjilo has received 1 Ring Of Charos from Huronjil0
Huronjilo has received 468 Dragon Dart from Huronjil0
Huronjilo has received 9 Crystal Key from Huronjil0

---2018/08/09 17:53:33---
Huronjilo has received 1 Pegasian Boots from Huronjil0
Huronjilo has received 1 Zaryte Bow from Huronjil0

---2018/08/09 18:15:44---
Huronjilo has received 1 Ring Of Charos(A) from Huronjil0

---2018/08/10 11:53:49---
Huronjilo has given 1 Samurai Shirt to Huronjil0
Huronjilo has given 1 Samurai Greaves to Huronjil0
Huronjilo has given 1 Samurai Boots to Huronjil0

---2018/08/10 11:55:33---
Huronjilo has received 750000 Blood Money from Huronjil0

---2018/08/10 14:50:34---
Huronjilo has received 1 3rd Age Cloak from Huronjil0
Huronjilo has received 1 Zuriels Robetop from Huronjil0
Huronjilo has received 1 Ancestral Robe Top from Huronjil0

---2018/08/10 14:51:46---
Huronjilo has given 1 Ring Of Charos(A) to Huronjil0
Huronjilo has given 1 Zuriels Robetop to Huronjil0
Huronjilo has given 1 3rd Age Cloak to Huronjil0
Huronjilo has given 1 Ancestral Robe Top to Huronjil0

---2018/08/11 11:50:24---
Huronjilo has given 1 Zaryte Bow to Huronjil0

---2018/08/11 14:26:35---
Huronjilo has received 1 Ring Of Charos(A) from Huronjil0

---2018/08/11 14:26:49---
Huronjilo has received 1 Zaryte Bow from Huronjil0

---2018/08/12 01:38:59---
Huronjilo has received 1 Dragon Hunter Crossbow from Huronjil0
Huronjilo has received 434 Ruby Bolts (E) from Huronjil0
Huronjilo has received 89 Dragon Bolts from Huronjil0
Huronjilo has received 350 Onyx Bolts (E) from Huronjil0

---2018/08/12 19:46:18---
Huronjilo has received 1 Ancestral Robe Bottom from Huronjil0
Huronjilo has received 1 Samurai Kasa from Huronjil0
Huronjilo has received 1 Team Cape X from Huronjil0
Huronjilo has received 1 Samurai Shirt from Huronjil0
Huronjilo has received 3 Tormented Bracelet from Huronjil0
Huronjilo has received 1 Samurai Greaves from Huronjil0
Huronjilo has received 1 Ancestral Robe Top from Huronjil0
Huronjilo has received 1 Nunchaku from Huronjil0
Huronjilo has received 2 Ring Of Suffering from Huronjil0
Huronjilo has received 1 Samurai Boots from Huronjil0

---2018/08/12 19:59:19---
Huronjilo has received 2 Armadyl Crossbow from Huronjil0

---2018/08/13 10:23:00---
Huronjilo has given 100000 Blood Money to Jabrone

---2018/08/13 10:23:00---
Huronjilo has received 1 Zuriels Hood from Jabrone

---2018/08/13 10:51:26---
Huronjilo has given 1000000 Blood Money to Jabrone

---2018/08/13 11:28:15---
Huronjilo has received 1 Armadyl Godsword from Michael Scot

---2018/08/13 11:29:33---
Huronjilo has given 430000 Blood Money to Jabrone

---2018/08/13 11:29:33---
Huronjilo has received 48 Castle Wars Ticket from Jabrone

---2018/08/13 11:31:16---
Huronjilo has received 10000000 Coins from Michael Scot

---2018/08/13 11:32:14---
Huronjilo has given 200000 Blood Money to Michael Scot

---2018/08/13 11:33:09---
Huronjilo has given 10 Mithril Seeds to Michael Scot

---2018/08/13 11:33:09---
Huronjilo has received 3000000 Coins from Michael Scot

---2018/08/13 11:35:46---
Huronjilo has given 1 Vesta'S Longsword to Michael Scot

---2018/08/13 11:40:06---
Huronjilo has given 1 Ring Of Charos to Michael Scot

---2018/08/13 11:42:34---
Huronjilo has received 26 Castle Wars Ticket from Scuzzy

---2018/08/13 11:42:34---
Huronjilo has given 200000 Blood Money to Scuzzy

---2018/08/13 11:52:08---
Huronjilo has given 1 Staff Of Light to Michael Scot

---2018/08/13 11:52:23---
Huronjilo has received 1 Staff Of Light from Michael Scot

---2018/08/13 11:58:38---
Huronjilo has given 1 Dark Bow to Michael Scot

---2018/08/13 11:58:38---
Huronjilo has received 175000 Blood Money from Michael Scot

---2018/08/14 09:27:49---
Huronjilo has received 20000000 Coins from Pat

---2018/08/14 09:29:07---
Huronjilo has received 49851763 Coins from Huronjil0

---2018/08/14 10:13:24---
Huronjilo has received 1 Karil'S Leatherskirt from Pat
Huronjilo has received 1 Pegasian Boots from Pat
Huronjilo has received 1 Archer Helm from Pat
Huronjilo has received 1 Team Cape X from Pat
Huronjilo has received 1 Necklace Of Anguish from Pat

---2018/08/14 15:48:44---
Huronjilo has received 20126340 Coins from Pat

---2018/08/14 15:49:17---
Huronjilo has received 50000000 Coins from Pat
